import { ConflictException, Injectable, InternalServerErrorException, UnauthorizedException } from '@nestjs/common';
import { LoginDTO, RegistrationDTO } from '../models/user.model';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '../entities/user.entity';
import { Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserEntity) private userRepo: Repository<UserEntity>,
    private jwtService: JwtService
  ) {}

  private mockUser = {
    email: 'jake@jake.jake',
    token: 'jwt.token.here',
    username: 'jake',
    bio: 'I work at softwarer dev',
    image: null,
  };

  async register(credentials: RegistrationDTO) {
    try {
      const user = this.userRepo.create(credentials);
      await user.save();
      const payload = { username: user.username };
      const token = this.jwtService.sign(payload);
      return {user: {...user.toJSON(), token}};
    } catch (err) {
      if (err.code === '23505') {
        throw new ConflictException('Username has already been taken!');
      }
      throw new InternalServerErrorException();
    }
  }

  async login({ email, password }: LoginDTO) {
    try {
      const user = await this.userRepo.findOne({ where: { email } });
      const isValid = (await user.comparePassword(password));
      if (!isValid) {
        console.log('There was error validation');
        throw new UnauthorizedException('Invalid credentials!');
      }
      const payload = { username: user.username };
      const token = this.jwtService.sign(payload);
      return { user: {...user.toJSON(), token} };
      } catch (err) {
        console.log('There was error');
      throw new UnauthorizedException('Invalid Credentials', err);
    }
  }
}
