import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '../entities/user.entity';
import { Like, Repository } from 'typeorm';
import { ArticleEntity } from '../entities/article.entity';
import { CreateArticleDTO, UpdateArticleDTO } from '../entities/article.model';
import { FindAllQuery, FindFeedQuery } from '../models/user.model';
import { TagEntity } from '../entities/tag.entity';

@Injectable()
export class ArticleService {
  constructor(
    @InjectRepository(UserEntity)
    private articleRepo: Repository<ArticleEntity>,
    @InjectRepository(UserEntity)
    private userRepo: Repository<UserEntity>,
    @InjectRepository(TagEntity) private tagRepo: Repository<TagEntity>
  ) {}

  private async upserTags(tagList: string[]) {
    const foundTags = await this.tagRepo.find({where: tagList.map(t => {
        tag: t;
      })});
    const newTags = tagList.filter(t => foundTags.map(t => t.tag).includes(t));
    await Promise.all(
        this.tagRepo.create(newTags.map(t => ({tag: t}))).map(t => t.save())
      );
  }

  async finAll(user: UserEntity, query: FindAllQuery) {
    let findOptions: any = {
      where: {

      }
    };
    if (query.author) {
      findOptions.where['author.username'] = query.author;
    }
    if (query.favorited) {
      findOptions.where['favoritedBy.username'] = query.favorited
    }
    if (query.tag) {
      findOptions.where['tagList'] = Like(`%${query.tag}%`)
    }
    if (query.offset) {
      findOptions.offset = query.offset;
    }
    if (query.limit) {
      findOptions.limit = query.limit;
    }
    return ( await ( this.articleRepo.find(findOptions) )).map
    (article => article.toArticle(user));
  }

  async findFeed(user: UserEntity, query: FindFeedQuery) {
    const { followee }
      = await this.userRepo.findOne({where: {id: user.id}, relations: ['followee']});
    const findOptions = {...query, where: followee.map(follow => {
      author: follow.id
      })};
    return (await this.articleRepo.find(findOptions)).map(
      article => article.toArticle(user)
    );
  }

  public findBySlug(slug: string) {
   return this.articleRepo.findOne({where: { slug }});
  }

  private  ensureOwnership(user: UserEntity, article: ArticleEntity): boolean {
    return article.author.id === user.id;
  }

  public async createArticle(user: UserEntity, data: CreateArticleDTO) {
    const article = this.articleRepo.create(data);
    article.author = user;
    await this.upserTags(data.tagList);
    const {slug} = await article.save();
    return (await this.articleRepo.findOne({slug})).toArticle(user);
  }

  public async updateArticle(slug: string, user: UserEntity, data: UpdateArticleDTO) {
   const article = await this.findBySlug(slug);
   if (!this.ensureOwnership(user, article)) {
     throw new UnauthorizedException();
   }

   await this.articleRepo.update({slug}, data);
   return article;
  }

  async deleteAcrticle(slug: string, user: UserEntity) {
    const article = await this.findBySlug(slug);
    if (!this.ensureOwnership(user, article)) {
      throw new UnauthorizedException();
    }
    await this.articleRepo.remove(article);
  }

  async favoriteArticle(slug: string, user: UserEntity) {
    const article = await this.findBySlug(slug);
    article.favoritedBy.push(user);
    await article.save();
    return (await this.findBySlug(slug)).toArticle(user);
  }

  async unfavortieArticle(slug: string, user: UserEntity) {
    const article = await this.findBySlug(slug);
    article.favoritedBy = article.favoritedBy.filter(fav => fav.id !== user.id);
    await article.save();
    return (await this.findBySlug(slug)).toArticle(user);
  }

}
