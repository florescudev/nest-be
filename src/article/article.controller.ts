import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards, ValidationPipe } from '@nestjs/common';
import { ArticleService } from './article.service';
import { AuthGuard } from '@nestjs/passport';
import { User } from '../auth/user.decorator';
import { UserEntity } from '../entities/user.entity';
import { CreateArticleDTO, UpdateArticleDTO } from '../entities/article.model';
import { OptionalAuthGuard } from '../auth/optional-auth.guard';
import { FindAllQuery, FindFeedQuery } from '../models/user.model';
import { CommentsService } from './comments.service';
import { CreateCommentDTO } from '../models/comment.model';

@Controller('articles')
export class ArticleController {
  constructor(
    private articleService: ArticleService,
    private commentService: CommentsService
  ) {}

  @Get()
  @UseGuards(new OptionalAuthGuard())
  async findAll(@User() user: UserEntity, @Query() query: FindAllQuery) {
    const articles = await this.articleService.finAll(user, query);
    return {articles, articlesCount: articles.length};
  }

  @Get('/feed')
  @UseGuards(AuthGuard())
  async findFeed(@User() user: UserEntity, @Query() query: FindFeedQuery) {
    const articles = await this.articleService.findFeed(user, query);
    return {articles, articlesCount: articles.length};
  }

  @Get('/:slug')
  @UseGuards(new OptionalAuthGuard())
  async findBySlug(@Param('slug' ) slug: string, @User() user: UserEntity) {
    const article = await this.articleService.findBySlug(slug);
    return {article: article.toArticle(user)};
  }

  @Post('/:slug')
  @UseGuards(AuthGuard())
  async createArticle(@User() user: UserEntity, @Body(ValidationPipe) data: {article: CreateArticleDTO}) {
    const article = await this.articleService.createArticle(user, data.article);
    return {article};
  }


  @Put('/:slug')
  @UseGuards(AuthGuard())
  async updateArticle(
    @Param('slug') slug: string,
    @User() user: UserEntity,
    @Body(ValidationPipe) data: {article: UpdateArticleDTO}
  ) {
    const article = await this.articleService.updateArticle(slug, user ,data.article);
    return { article };
  }

  @Delete('/:slug')
  @UseGuards(AuthGuard())
  async deleteArticle(
    @Param() slug: string,
    @User() user: UserEntity,
  ) {
    const article = await this.articleService.deleteAcrticle(slug, user);
    return {article};
  }

  @Get('/:slug/comments')
  async findComments(@Param('slug') slug: string) {
    const comments = await this.commentService.findByArticleSlug(slug);
    return comments;
  }

  @Post('/:slug/comments')
  async createComments(
    @User() user: UserEntity,
    @Body(ValidationPipe) data: { comment: CreateCommentDTO }
    ) {
    const comment = await this.commentService.createComments(user, data.comment);
    return { comment };
  }

  @Delete('/:slug/comments/:id')
  async deleteComment(
    @User() user: UserEntity,
    @Param('id') id: number,
  ) {
    const comment = await this.commentService.deleteComments(user, id);
    return { comment };
  }

  @Post('/:slug/:favorite')
  @UseGuards(AuthGuard())
  async favoriteArticle(
  @Param('slug') slug: string,
  @User() user: UserEntity
  ) {
    const article = await this.articleService.favoriteArticle(slug, user);
    return {article};
  }

  @Delete('/:slug/:favorite')
  async unfavoriteArticle(
    @Param('slug') slug: string,
    @User() user: UserEntity
  ) {
    const article = await this.articleService.unfavortieArticle(slug, user);
    return {article};
  }


}
